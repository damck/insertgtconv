#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <windows.h>
#include <string>

using namespace std;

string openfilename(char *filter = "All Files (*.*)\0*.*\0", HWND owner = nullptr) {

	OPENFILENAME ofn;
	char fileName[MAX_PATH] = "";
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = owner;
	ofn.lpstrFilter = filter;
	ofn.lpstrFile = fileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = "";
	string fileNameStr;
	if (GetOpenFileName(&ofn))
		fileNameStr = fileName;
	return fileNameStr;
}

void split(const string &s, char delim, vector<string> &elems) {
    stringstream ss;
    ss.str(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
}


vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

struct LineData{
    string invoice;
    string companyname;
    string street;
    string city;
    string zip;
    string nip;
    string warehouse = "CENTRALA";
    string customernum;
    string date_print;
    string date_sale;
    string date_paid;
    vector<vector<string>> vat;
    string vatsum;
    string nettosum;
    string bruttosum;

    void set_sums(){
        float sum1 = 0;
        float sum2 = 0;
        float sum3 = 0;
        for(auto vat: this->vat){
            if(vat[1][0]!='0')
                sum1+=stof(vat[1]);
            if(vat[2][0]!='0')
                sum2+=stof(vat[2]);
            if(vat[3][0]!='0')
                sum3+=stof(vat[3]);
        }
        stringstream stream;
        stream << fixed << setprecision(4) << sum1;
        this->nettosum = stream.str();
        stream.str("");
        stream << fixed << setprecision(4) << sum2;
        this->vatsum = stream.str();
        stream.str("");
        stream << fixed << setprecision(4) << sum3;
        this->bruttosum = stream.str();
        stream.str("");
    }


};

string fix_quotes(string input)
{
	replace(input.begin(), input.end(), '\"', '\'');

	return input;
}

string fixdate(string olddate){
    stringstream stream;
    vector<string> date_sep;
    split(olddate,'-', date_sep);
    stream << date_sep[2] << date_sep[1] << date_sep[0] << "000000";
    return stream.str();
}


void myparse(ifstream* infile){
    string line;
    vector<string> clients;
    ofstream outFile;
	//cout << "Podaj plik z informacja o firmie:" << endl;
    //ifstream infoFile(openfilename().c_str());
	//cout << "Podaj nazwe pliku wyjsciowego: ";
	ifstream infoFile("firma.txt");
	if(infoFile.fail())
	{
		cout<<"Plik z naglowkiem powinien sie nazywac \"firma.txt\" i byc w tym samym folderze co program"<<endl;
		//cout<<"Nacisnij enter by przerwac dzialanie"<<endl;
		system("pause");
		infoFile.close();
		infile->close();
		exit(0);
	}
	string filename = "output";
	//cin >> filename;
    outFile.open(filename+".epp");
    while(getline(infoFile,line)){
        outFile<<line<<endl;
    }
    outFile << endl;
    while(getline(*infile,line)){
        LineData data;
        vector<string> raw_data;
        split(line.substr(3), ';', raw_data);
        data.invoice = raw_data[0];
        data.companyname = fix_quotes(raw_data[5]);
        data.street = raw_data[6];
        data.city = raw_data[7];
        data.zip = raw_data[8];
        data.nip = raw_data[9];
        data.customernum = raw_data[42];
        data.date_print = fixdate(raw_data[1]);
        data.date_sale = fixdate(raw_data[48]);
        data.date_paid = fixdate(raw_data[49]);
        for(int i = 16; i < 28; i++){           //get vat values
            vector<string> vat_temp;
            split(raw_data[i],':', vat_temp);
                if(vat_temp[1][0]!='0'){
                    data.vat.push_back(vat_temp);
                }
        }
        for(vector<vector<string>>::iterator it = data.vat.begin();     //get brutto
                it != data.vat.end(); ++it){
            float tmp;
            string s1 = (*it)[1];
            string s2 = (*it)[2];
            tmp = stof(s1) + stof(s2);
            stringstream stream;
            stream << fixed << setprecision(4) << tmp;
            (*it).push_back(stream.str());
        }
        data.set_sums();
//        cout<<"[NAGLOWEK]"<<endl;
        outFile << "[NAGLOWEK]" << endl;
        stringstream output;
        output << "\"" << "FS" << "\"" << ",1,0,"       //header
               << split(data.invoice,'/')[0]<< ",\""
               << data.invoice << "\",,"
               << "\"" << data.invoice << "\",,,,,"
               << "\"" << data.nip << "\",\"" << data.companyname
               << "\",\"" << data.companyname
               << "\",\"" << data.city << "\"," "\"" << data.zip << "\","
               << "\"" << data.street << "\","
               << "\"" << data.nip << "\","
               << "\"" << data.warehouse << "\",,,"
               << data.date_print << "," <<data.date_sale << ","
               << data.date_paid << ",1,1,," << data.nettosum
               << "," << data.vatsum << "," << data.bruttosum
               << ",0.0000,,0.0000,\"Przelewem\","<<data.date_paid<<","
               << data.bruttosum << "," << data.bruttosum
               << ",0,0,1,0,,\"Sprzedawca\",,0.0000,0.0000,\"PLN\",1.0000"
               << ",,,,,0,0,0,\"POS nr 1\",0.0000,\"12 miesiecy\",0.0000,,,0"<<endl;
        outFile << output.str();
        output.str("");
        outFile << "[ZAWARTOSC]" << endl;          //get contents
        for(auto vat: data.vat){
            string tmp = vat[0];
            string tmp2;
            if (tmp[0] > 47 && tmp[0] < 58){
                tmp.erase(tmp.size()-1);
                tmp2 = tmp;
            }
            if(tmp[0] == 'z') tmp2 = "-1";
            output << "\"" << tmp << "\","
                   << tmp2 << ","
                   << vat[1] << ","
                   << vat[2] << ","
                   << vat[3] << endl;
            outFile << output.str();
            output.str("");
        }

        outFile << endl;
        //add a client to the vector
        stringstream client;
        client << "0,\"" << data.nip << "\","
               << "\"" << data.companyname << "\",\""
               << data.companyname <<"\",\""
               << data.city << "\",\"" << data.zip
               << "\",\"" << data.street << "\",\""
               << data.nip << "\""
               << ",,,,,,,,\"" << data.customernum << "\",\""
               << data.customernum << "\",,,,,,,,,,,,,0"<<endl;
        clients.push_back(client.str());
        client.str("");
    }

    outFile << "[NAGLOWEK]"  << endl
         << "\"KONTRAHENCI\"" << endl;
    outFile << "[ZAWARTOSC]" << endl;

    for (auto client : clients){
        outFile << client;
    }

    outFile.close();
}

int main() {
	cout << "Podaj plik z fakturami:" << endl;
	ifstream infile(openfilename().c_str());
    myparse(&infile);
	
    infile.close();
	cout<<"Plik wynikowy w \"output.epp\""<<endl;
	system("pause");
    return 0;
}